#![cfg_attr(not(feature = "std"), no_std)]

use ink_lang as ink;

#[ink::contract]
mod transfer {
    use ink_env::{ transfer, DefaultEnvironment};

    #[cfg(not(feature = "ink-as-dependency"))]
    #[ink(storage)]
    pub struct Transfer {
        total_supply: Balance,
        balances: ink_storage::collections::HashMap<AccountId, Balance>,
    }

    impl Transfer {
        #[ink(constructor)]
        pub fn new(initial_supply: Balance) -> Self {
            let mut balances = ink_storage::collections::HashMap::new();
            balances.insert(Self::env().caller(), initial_supply);
            Self {
                total_supply: initial_supply,
                balances,
            }
        }

        #[ink(message)]
        pub fn total_supply(&self) -> Balance {
            self.total_supply
        }

        #[ink(message)]
        pub fn balance_of(&self, owner: AccountId) -> Balance {
            self.balance_of_or_zero(&owner)
        }

        #[ink(message)]
        pub fn transfer(&mut self, to: AccountId, value: Balance) -> bool {
            // ACTION: Call the `transfer_from_to` with `from` as `self.env().caller()`
            self.transfer_from_to(self.env().caller(), to, value)
        }

        fn transfer_from_to(&mut self, from: AccountId, to: AccountId, value: Balance) -> bool {
            let mut from_balance = self.balance_of_or_zero(&from);
            let mut to_balance = self.balance_of_or_zero(&to);

            if from_balance < value {
                false
            } else {

                from_balance -= value;
                to_balance += value;
                transfer::<DefaultEnvironment>(to,value).is_ok()
            }
        }

        fn balance_of_or_zero(&self, owner: &AccountId) -> Balance {
            *self.balances.get(owner).unwrap_or(&0)
        }
    }

    #[cfg(test)]
    mod tests {
        use ink_lang as ink;
        use ink_lang::static_assertions::_core::convert::TryFrom;

        use super::*;

        #[ink::test]
        fn new_works() {
            let contract = Transfer::new(777);
            assert_eq!(contract.total_supply(), 777);
        }

        #[ink::test]
        fn balance_works() {
            let contract = Transfer::new(100);
            assert_eq!(contract.total_supply(), 100);
            assert_eq!(contract.balance_of(AccountId::try_from(&[1_u8; 32])), 100);
            assert_eq!(contract.balance_of(AccountId::try_from(&[0_u8; 32])), 0);
        }

        #[ink::test]
        fn transfer_works() {
            let mut contract = Erc20::new(100);
            assert_eq!(contract.balance_of(AccountId::try_from(&[0x1; 32])), 100);
            assert!(contract.transfer(AccountId::try_from(&[0x0; 32]), 10));
            assert_eq!(contract.balance_of(AccountId::try_from(&[0x0; 32])), 10);
            assert!(!contract.transfer(AccountId::try_from(&[0x0; 32]), 100));
        }
    }
}
